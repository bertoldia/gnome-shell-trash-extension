release:
	zip -r gnome-shell-trash-extension.zip * -x .git -x .zip -x Makefile -x tags

clean:
	rm -f gnome-shell-trash-extension.zip
